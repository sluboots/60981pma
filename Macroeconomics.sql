-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Май 16 2021 г., 20:02
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `Macroeconomics`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Economics`
--

CREATE TABLE `Economics` (
  `ID_Economics` int NOT NULL,
  `ID_Country` int NOT NULL,
  `YEAR` varchar(256) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `GDP` varchar(256) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `GDP per Person` varchar(256) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `Economics`
--

INSERT INTO `Economics` (`ID_Economics`, `ID_Country`, `YEAR`, `GDP`, `GDP per Person`) VALUES
(1, 1, '2021', '4.136 трлн $', '28.184 $'),
(2, 2, '2021', '21.433 трлн $', '65.253 $'),
(3, 3, '2020', '27.865 трлн $', '19.786 $'),
(4, 4, '2018', '36.789 трлн $', '67.987 $'),
(5, 7, '1990', '7.987 трлн $', '6758 $'),
(6, 7, '2021', '45.869 трлн $', '140.000 $');

-- --------------------------------------------------------

--
-- Структура таблицы `Essence`
--

CREATE TABLE `Essence` (
  `ID_Country` int NOT NULL,
  `Country` varchar(256) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `Capital` varchar(256) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `Population` varchar(256) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `Area` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `Essence`
--

INSERT INTO `Essence` (`ID_Country`, `Country`, `Capital`, `Population`, `Area`) VALUES
(1, 'Russian Federation', 'Moscow', '147.250.000', '17 125 191 км² '),
(2, 'USA', 'Washington DC', '331.449.281', '9.519.431 км²'),
(3, 'Switzerland', 'Bern', '8.558.758', '41.284 км²'),
(4, 'Germany', 'Berlin', '83.019.000 чел.', '357.385 км²'),
(5, 'Canada', 'Ottawa', '37.602.103', '9.984.670 км²'),
(6, 'Qatar', 'Doha', '2.638.657', '11.586 км²'),
(7, 'Kuwait', 'Kuwait City', '4.464.521', '17.818 км²');

-- --------------------------------------------------------

--
-- Структура таблицы `Event`
--

CREATE TABLE `Event` (
  `ID_Event` int NOT NULL,
  `ID_Country` int NOT NULL,
  `DATE` varchar(256) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `Event` varchar(256) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `Event`
--

INSERT INTO `Event` (`ID_Event`, `ID_Country`, `DATE`, `Event`) VALUES
(1, 5, '2021-01-01', 'New Year'),
(2, 6, '1876-12-09', 'Saint\'s Petr Day'),
(3, 7, '2005-11-25', 'Found A New Oil field'),
(4, 3, '2020-07-21', 'Add New Trade Way');

-- --------------------------------------------------------

--
-- Структура таблицы `Trade`
--

CREATE TABLE `Trade` (
  `ID_Trade` int NOT NULL,
  `ID_Country1` int NOT NULL,
  `ID_Country2` int NOT NULL,
  `Export_From_Country1_To_Country2` varchar(256) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `Export_From_Country2_To_Country1` varchar(256) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `Trade`
--

INSERT INTO `Trade` (`ID_Trade`, `ID_Country1`, `ID_Country2`, `Export_From_Country1_To_Country2`, `Export_From_Country2_To_Country1`) VALUES
(1, 2, 5, '54.766 трлн $', '14.047 трлн $'),
(2, 6, 1, '89.766 трлн $', '76.336 трлн $'),
(3, 4, 3, '21.066 трлн $', '54.766 трлн $'),
(4, 6, 3, '54.766 трлн $', '167. 906 трлн $'),
(5, 1, 2, '54.766 трлн $', '268.766 трлн $');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Economics`
--
ALTER TABLE `Economics`
  ADD PRIMARY KEY (`ID_Economics`),
  ADD KEY `ID_Country` (`ID_Country`);

--
-- Индексы таблицы `Essence`
--
ALTER TABLE `Essence`
  ADD PRIMARY KEY (`ID_Country`);

--
-- Индексы таблицы `Event`
--
ALTER TABLE `Event`
  ADD PRIMARY KEY (`ID_Event`),
  ADD KEY `ID_Country` (`ID_Country`);

--
-- Индексы таблицы `Trade`
--
ALTER TABLE `Trade`
  ADD PRIMARY KEY (`ID_Trade`),
  ADD KEY `ID_Country1` (`ID_Country1`),
  ADD KEY `Trade_ibfk_2` (`ID_Country2`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Economics`
--
ALTER TABLE `Economics`
  MODIFY `ID_Economics` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `Essence`
--
ALTER TABLE `Essence`
  MODIFY `ID_Country` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `Event`
--
ALTER TABLE `Event`
  MODIFY `ID_Event` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `Trade`
--
ALTER TABLE `Trade`
  MODIFY `ID_Trade` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `Economics`
--
ALTER TABLE `Economics`
  ADD CONSTRAINT `Economics_ibfk_1` FOREIGN KEY (`ID_Country`) REFERENCES `Essence` (`ID_Country`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Event`
--
ALTER TABLE `Event`
  ADD CONSTRAINT `Event_ibfk_1` FOREIGN KEY (`ID_Country`) REFERENCES `Essence` (`ID_Country`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Trade`
--
ALTER TABLE `Trade`
  ADD CONSTRAINT `Trade_ibfk_1` FOREIGN KEY (`ID_Country1`) REFERENCES `Essence` (`ID_Country`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Trade_ibfk_2` FOREIGN KEY (`ID_Country2`) REFERENCES `Essence` (`ID_Country`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
