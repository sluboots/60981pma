<?php namespace App\Models;
use CodeIgniter\Model;

class TradeModel extends Model
{
    protected $table = 'Trade'; //таблица, связанная с моделью
    public function getTrade($id = null)
    {
        if (!isset($id))
        {
            return $this->findAll();
        }
        return $this->where(['ID_Trade' => $id])->first();

    }
}