<?php namespace App\Models;
use CodeIgniter\Model;

class EcoModel extends Model
{
    protected $table = 'Economics'; //таблица, связанная с моделью\
    protected $allowedFields = ['Id_Country', 'YEAR', 'GDP', 'GDP_per_Person'];
    public function getEco($id = null)
    {
        if (!isset($id))
        {
            return $this->findAll();
        }
        return $this->where(['Id_Country' => $id])->first();

    }

    public function getCountry($id = null)
    {
        return $this->where(['Id_Country' => $id])->findAll();
    }

    public function GetEcoWithUser($id = null, $search = '')
    {
        $builder = $this->select('*')->join('users','Economics.ID_User = users.id');
        if (!is_null($id))
        {
            return $builder->where(['Essence.ID_Country' => $id])->first();
        }
        return $builder;
    }

}

