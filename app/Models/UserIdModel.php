<?php namespace App\Models;
use CodeIgniter\Model;

class UserIdModel extends Model
{
    protected $table = 'users'; //таблица, связанная с моделью
    public function getUser($id = null)
    {
        if (!isset($id))
        {
            return $this->findAll();
        }
        return $this->where(['ID_Country' => $id])->first();

    }
}

