<?php namespace App\Models;
use CodeIgniter\Model;
class CountryModel extends Model
{
    protected $table = 'Essence'; //таблица, связанная с моделью
    protected $allowedFields = ['Country', 'Capital', 'Population', 'Area', 'ID_User', 'picture_url'];
    public function getCountry($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['ID_Country' => $id])->first();
    }
    public function getCountryWithUser($id = null, $search = '')
    {
        /*$builder = $this->select('*')->join('users','Essence.ID_User = users.id')->like('Country', $search,'both', null, true)->orlike('Capital',$search,'both',null,true);
        if (!is_null($id))
        {
            return $builder->where(['Essence.ID_Country' => $id])->first();
        }
        return $builder;
        */
        $builder = $this->select('*, Essence.ID_Country')->join('users','Essence.ID_User = users.id');
        $builder = $this->like('Essence.Country', $search)->orlike('Essence.Capital',$search);
        if (!is_null($id))
        {
            return $builder->where(['Essence.ID_Country' => $id])->first();
        }
        return $builder;
    }

    public function upddata($data, $id)
    {
        $this->db->table($this->table)->where('ID_Country', $id['ID_Country'])->update($data);
        /*$ID_Country = $data['ID_Country'];
        $this->db->where($ID_Country, $data[ID_Country])->first();
        $this->db->update('Essence' ,$data);
        return true;*/
    }
}
