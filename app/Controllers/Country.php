<?php namespace App\Controllers;
use App\Models\CountryModel;
use App\Models\EcoModel;
use App\Models\TradeModel;
use IonAuth\Libraries\IonAuth;
use Aws\S3\S3Client;

class Country extends BaseController


{
    public function index() //Обображение всех записей
 {
     if (!$this->ionAuth->loggedIn())
     {
         return redirect()->to('/auth/login_user');
     }
   $model = new CountryModel();
   $data['Essence'] = $model->getCountry();
   echo view('country/view_all', $this->withIon($data));
 }

 public function view($id = null) //отображение одной записи
 {
     if (!$this->ionAuth->loggedIn())
     {
         return redirect()->to('/auth/login_user');
     }
   $model = new CountryModel();
   $data['Essence'] = $model->getCountry($id);
   $data['Essence_new'] = $model->getCountry();
   $model = new EcoModel();
   $data['Economics'] = $model->getCountry($id);
   $model = new TradeModel();
   $data['Trade'] = $model->getTrade();
   echo view('country/view', $this->withIon($data));
 }

 public function viewAllWithCountry()
 {
     if ($this->ionAuth->isAdmin())
     {
         //Подготовка значения количества элементов выводимых на одной странице
         if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
         {
             //сохранение кол-ва страниц в переменной сессии
             session()->setFlashdata('per_page', $this->request->getPost('per_page'));
             $per_page = $this->request->getPost('per_page');
         }
         else {
             $per_page = session()->getFlashdata('per_page');
             session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
             if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
         }
         $data['per_page'] = $per_page;

         //Обработка запроса на поиск
         if (!is_null($this->request->getPost('search')))
         {
             session()->setFlashdata('search', $this->request->getPost('search'));
             $search = $this->request->getPost('search');
         }
         else {
             $search = session()->getFlashdata('search');
             session()->setFlashdata('search', $search);
             if (is_null($search)) $search = '';
         }
         $data['search'] = $search;

         helper(['form','url']);

         $model = new CountryModel();
         $data['Essence'] = $model->getCountryWithUser(null, $search)->paginate($per_page, 'group1');
         $data['pager'] = $model->pager;
         echo view('country/view_all_with_country', $this->withIon($data));
         }
         else
         {
             //session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
             return redirect()->to('/auth/login_user');
         }
     }


    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login_user');
        }
        helper(['form']);

        $model = new CountryModel();
        $data ['validation'] = \Config\Services::validation();
        $data['Essence'] = $model->getCountryWithUser();
        $data['ionAuth'] = new IonAuth();

        echo view('country/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'Country' => 'required|min_length[3]|max_length[255]',
                'Capital'  => 'required',
                'Population'  => 'required|integer',
                'Area'  => 'required|integer',
                'ID_User' => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }
            $model = new CountryModel();
            $data = [
                'Country' => $this->request->getPost('Country'),
                'Capital' => $this->request->getPost('Capital'),
                'Population' => $this->request->getPost('Population'),
                'Area' => $this->request->getPost('Area'),
                'ID_User' => $this->request->getPost('ID_User'),
            ];
            if (!is_null($insert))
                $data['picture_Url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Curating.country_create_success'));
            return redirect()->to('/country');
        }
        else
        {
            return redirect()->to('/country/create')->withInput();
        }
    }
    public function create_GDP()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login_user');
        }

        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        $model = new CountryModel();
        $data['Essence'] = $model->getCountry();
        echo view('country/create_GDP', $this->withIon($data));
    }
    public function store_GDP($id = null)
    {
        helper(['form','url']);
        $id = ['ID_Country' => $this->request->getPost('ID_Country')];
        if ($this->request->getMethod() === 'post' && $this->validate([
                'YEAR'  => 'required',
                'GDP'  => 'required',
                'GDP_per_Person'  => 'required',
                'Id_Country' => 'required',
            ]))
        {
            $model = new EcoModel();
            $data = [
                'Id_Country' => $this->request->getPost('Id_Country'),
                'YEAR'  => $this->request->getPost('YEAR'),
                'GDP'  => $this->request->getPost('GDP'),
                'GDP_per_Person'  => $this->request->getPost('GDP_per_Person'),
            ];
            $model->save($data);
            session()->setFlashdata('message', lang('Curating.country_create_success'));
            return redirect()->to('/country');
        }
        else
        {
            return redirect()->to('/country/create_GDP')->withInput();
        }
    }
    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login_user');
        }
        $model = new CountryModel();

        helper(['form']);
        $data ['Essence'] = $model->getCountry($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('country/edit', $this->withIon($data));

    }
    public function updateCountry()
    {
        helper(['form','url']);
        echo '/country/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'ID_Country' => 'required',
                'Country' => 'required|min_length[3]|max_length[255]',
                'Capital'  => 'required',
                'Population'  => 'required|integer',
                'Area'  => 'required|integer',

            ]))
        {

            $model = new CountryModel();
            $id = ['ID_Country' => $this->request->getPost('ID_Country')];
            $data = [
                'Country' => $this->request->getPost('Country'),
                'Capital' => $this->request->getPost('Capital'),
                'Population' => $this->request->getPost('Population'),
                'Area' => $this->request->getPost('Area'),
                'ID_User' => $this->ionAuth->user()->row()->id,
            ];
            $model->upddata($data, $id);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/country/view/'.$this->request->getPost('ID_Country'));
        }
        else
        {
            return redirect()->to('/country/edit/'.$this->request->getPost('ID_Country'))->withInput();
        }
    }
    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login_user');
        }

        $model = new EcoModel();
        $model->where('Id_Country', $id)->delete();
        $model = new TradeModel();
        $model->where('Id_Country1', $id)->delete();
        $model = new TradeModel();
        $model->where('Id_Country2', $id)->delete();
        $model = new CountryModel();
        $model->where('ID_Country', $id)->delete();


        //$model->delete(12);
        return redirect()->to('/country/viewAllWithCountry');
    }


}


