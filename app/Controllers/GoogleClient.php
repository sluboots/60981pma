<?php
namespace App\Controllers;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('309889775777-0hn4pt3r62uv3a7bh5f5jrqgm85i747l.apps.googleusercontent.com');
        $this->google_client->setClientSecret('pn3Iid4QeiI0mT8YplEOc2Mk');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }

}