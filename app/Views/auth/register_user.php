
<?= $this->extend('templates/layout'); echo strlen($message)?>
<?= $this->section('content') ?>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-12 ">
                <h1>Регистрация</h1>
                <?php if (isset($message)): ?>
                    <div class="alert alert-danger">
                        <?php echo $message;?>
                    </div>
                <?php endif ?>
                <?php echo form_open('auth/register_user');?>
                <div class="mb-3">
                    <?php echo form_label(lang('Имя'), 'first_name');?> <br />
                    <?php echo form_input($first_name);?>
                </div>
                <div class="mb-3">
                    <?php echo form_label(lang('Фамилия'), 'last_name');?> <br />
                    <?php echo form_input($last_name);?>
                </div>
                <?php
                if ($identity_column !== 'email') {
                    echo '<div class="mb-3">';
                    echo form_label(lang('Auth.create_user_identity_label'), 'identity');
                    echo '<br/>';
                    echo \Config\Services::validation()->getError('identity');
                    echo form_input($identity);
                    echo '</div>';
                }
                ?>
                <div class="mb-3">
                    <?php echo form_label(lang('Email'), 'email');?> <br />
                    <?php echo form_input($email);?>
                </div>
                <div class="mb-3">
                    <?php echo form_label(lang('Номер телефона'), 'phone');?> <br />
                    <?php echo form_input($phone);?>
                </div>
                <div class="mb-3">
                    <?php echo form_label(lang('Пароль'), 'password');?> <br />
                    <?php echo form_input($password);?>
                </div>
                <div class="mb-3">
                    <?php echo form_label(lang('Подтвердить пароль'), 'password_confirm');?> <br />
                    <?php echo form_input($password_confirm);?>
                </div>
                <div class="mb-3">
                    <?php echo form_submit('submit', lang('Зарегистрировать пользователя'), 'class="btn btn-primary bg-dark" style="border-color: #222222"');?>
                </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>