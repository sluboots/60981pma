<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="jumbotron text-center">
        <img class="mb-4" src="https://img.icons8.com/cotton/64/000000/earth-planet--v1.png"" alt="" width="72" height="72"><h1 class="display-4">MacroEconomics</h1>
        <p class="lead">Данное веб-приложение позволит узнать общую экономическую информацию о странах, а также последние финансовые новости.</p>
        <?php if (!$ionAuth->loggedIn()): ?>
        <a class="btn btn-primary btn-lg bg-dark" style="border-color: #222222" href="<?php echo base_url()?>/auth/login_user" role="button">Войти</a>
        <?php endif?>
    </div>
<?= $this->endSection() ?>

<div>Icons made by <a href="https://www.flaticon.com/authors/turkkub" title="turkkub">turkkub</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>