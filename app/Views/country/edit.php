<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('country/updateCountry'); ?>
    <div class="form-group">
        <input type="hidden" name="ID_Country" value="<?= $Essence["ID_Country"] ?>">
        <label for="name">Страна</label>
        <input type="text" class="form-control <?= ($validation->hasError('Country')) ? 'is-invalid' : ''; ?>" name="Country"
               value="<?= $Essence["Country"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('Country') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="name">Столица</label>
        <input type="text" class="form-control <?= ($validation->hasError('Capital')) ? 'is-invalid' : ''; ?>" name="Capital"
               value="<?= $Essence["Capital"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('Capital') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="name">Популяция</label>
        <input type="text" class="form-control <?= ($validation->hasError('Population')) ? 'is-invalid' : ''; ?>" name="Population"
               value="<?= $Essence["Population"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('Population') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="name">Площадь</label>
        <input type="text" class="form-control <?= ($validation->hasError('Area')) ? 'is-invalid' : ''; ?>" name="Area"
               value="<?= $Essence["Area"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('Area') ?>
        </div>

    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary bg-dark " style="border-color: #222222" name="submit">Сохранить</button>
    </div>
    <input type="hidden" name="ID_User" value="<?= $Essence["ID_User"] ?>">
    </form>


</div>
<?= $this->endSection() ?><?php
