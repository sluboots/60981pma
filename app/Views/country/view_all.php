<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="album py-5 bg-light">
    <div class="container">
        <h2>Все страны</h2>
        <div class="row row-cols-2 row-cols-md-4">
             <?php if (!empty($Essence) && is_array($Essence)) : ?>

                     <?php foreach ($Essence as $item): ?>
                    <div class="card mb-3 h-90 w-50" style="">
                        <div class="row " style="">
                            <div class="col-md-3">
                                <?php if ($item['Country'] == 'Russian Federation') : ?>
                                    <img src="https://img.icons8.com/emoji/96/000000/russia-emoji.png" class="card-img" style="margin-left: 25px; margin-top: 25px; min-width: 100px" alt="<?= esc($item['Country']); ?>">
                                <?php elseif ($item['Country'] == 'USA'):?>
                                    <img src="https://img.icons8.com/cute-clipart/96/000000/usa.png" class="card-img" style="margin-left: 25px; margin-top: 25px; min-width: 100px" alt="<?= esc($item['Country']); ?>">
                                <?php elseif ($item['Country'] == 'Switzerland'):?>
                                    <img src="https://img.icons8.com/emoji/96/000000/switzerland-emoji.png" class="card-img" style="margin-left: 25px; margin-top: 20px; min-width: 100px" alt="<?= esc($item['Country']); ?>">
                                <?php elseif ($item['Country'] == 'Germany'):?>
                                    <img src="https://img.icons8.com/emoji/96/000000/germany-emoji.png" class="card-img" style="margin-left: 25px; margin-top: 25px; min-width: 100px" alt="<?= esc($item['Country']); ?>">
                                <?php elseif ($item['Country'] == 'Canada'):?>
                                    <img src="https://img.icons8.com/emoji/96/000000/canada-emoji.png" class="card-img" style="margin-left: 25px; margin-top: 25px; min-width: 100px" alt="<?= esc($item['Country']); ?>">
                                <?php elseif ($item['Country'] == 'Qatar'):?>
                                    <img src="https://img.icons8.com/emoji/96/000000/qatar-emoji.png" class="card-img" style="margin-left: 25px; margin-top: 25px; min-width: 100px" alt="<?= esc($item['Country']); ?>">
                                <?php elseif ($item['Country'] == 'Kuwait'):?>
                                    <img src="https://img.icons8.com/emoji/96/000000/kuwait-emoji.png" class="card-img" style="margin-left: 25px; margin-top: 25px; min-width: 100px" alt="<?= esc($item['Country']); ?>">
                                <?php else : ?>
                                    <img src="<?= esc($item['picture_Url']); ?>" class="card-img" style="margin-left: 25px; margin-top: 25px; min-width: 100px" alt="<?= esc($item['Country']); ?>">
                                <?php endif ?>

                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title"><?= esc($item['Country']); ?></h5>
                                    <p class="card-text">Столица: <?= esc($item['Capital']); ?></p>
                                    <a href="<?= base_url()?>/country/view/<?= esc($item['ID_Country']); ?>" class="btn bg-dark btn-primary" style="border-color: #222222">Просмотреть</a>
                                </div>
                            </div>
                        </div>
                    </div>

            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти страны.</p>
        <?php endif ?>
    </div>
</div>

<?= $this->endSection() ?>
