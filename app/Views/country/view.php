<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="album py-5 bg-light">
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($Essence)) : ?>

            <div class="row">
                <div class="col-sm-6 " style="max-width: 540px;">
                    <div class="card mb-3 h-28">
                        <div class="col-md-4 d-flex align-items-center">
                            <?php if ($Essence['Country'] == 'Russian Federation') : ?>
                                <img src="https://img.icons8.com/emoji/96/000000/russia-emoji.png" class="card-img" style="margin-left: 25px" alt="<?= esc($Essence['Country']); ?>">
                            <?php elseif ($Essence['Country'] == 'USA'):?>
                                <img src="https://img.icons8.com/cute-clipart/96/000000/usa.png" class="card-img" style="margin-left: 25px" alt="<?= esc($Essence['Country']); ?>">
                            <?php elseif ($Essence['Country'] == 'Switzerland'):?>
                                <img src="https://img.icons8.com/emoji/96/000000/switzerland-emoji.png" class="card-img" style="margin-left: 25px" alt="<?= esc($Essence['Country']); ?>">
                            <?php elseif ($Essence['Country'] == 'Germany'):?>
                                <img src="https://img.icons8.com/emoji/96/000000/germany-emoji.png" class="card-img" style="margin-left: 25px" alt="<?= esc($Essence['Country']); ?>">
                            <?php elseif ($Essence['Country'] == 'Canada'):?>
                                <img src="https://img.icons8.com/emoji/96/000000/canada-emoji.png" class="card-img" style="margin-left: 25px" alt="<?= esc($Essence['Country']); ?>">
                            <?php elseif ($Essence['Country'] == 'Qatar'):?>
                                <img src="https://img.icons8.com/emoji/96/000000/qatar-emoji.png" class="card-img" style="margin-left: 25px" alt="<?= esc($Essence['Country']); ?>">
                            <?php elseif ($Essence['Country'] == 'Kuwait'):?>
                                <img src="https://img.icons8.com/emoji/96/000000/kuwait-emoji.png" class="card-img" style="margin-left: 25px" alt="<?= esc($Essence['Country']); ?>">
                            <?php else : ?>
                                <img src="<?= esc($Essence['picture_Url']); ?>" class="card-img" style="margin-left: 25px; margin-top: 25px; min-width: 100px" alt="<?= esc($Essence['Country']); ?>">
                            <?php endif ?>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= esc($Essence['Country']); ?></h5>
                                <p class="card-text">Столица: <?= esc($Essence['Capital']); ?></p>
                                <div class="d-flex justify-content-between">
                                    <div class="my-0">Популяция:</div>
                                    <div class="text-muted"><?= esc($Essence['Population']); ?> человек</div>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <div class="my-0">Площадь страны:</div>
                                    <div class="text-muted"><?= esc($Essence['Area']); ?> км²</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if (!empty($Economics)) : ?>
                        <?php foreach ($Economics as $items): ?>
                            <?php if ($items['Id_Country'] == $Essence['ID_Country'])  : ?>
                                <div class="card mb-3" style="max-width: 540px;">
                                    <div class="">
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title">ВВП страны за: <?= esc($items['YEAR']); ?></h5>
                                                <p class="card-text">ВВП: <?= esc($items['GDP']); ?> $</p>
                                                <div class="d-flex justify-content-between">
                                                    <p class="card-text">ВВП на душу населения: <?= esc($items['GDP_per_Person']); ?> $</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ?>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <p>Отсутствует информация об экономике.</p>
                <?php endif ?>
            </div>
                <?php else : ?>
                <p>Рейтинг не найден.</p>
            <?php endif ?>
            <?php if (!empty($Trade) && is_array($Trade) && (!empty($Essence_new) && is_array($Essence_new))) : ?>
                <div class="col-sm-6 " style="max-width: 540px;">
                    <?php foreach ($Trade as $items): ?>
                        <?php if ($items['Id_Country1'] == $Essence['ID_Country'])  : ?>
                            <?php foreach ($Essence_new as $item): ?>
                                <?php if ($items['Id_Country2'] == $item['ID_Country'])  : ?>
                                    <div class="card mb-3" style="max-width: 540px;">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="card-body">
                                                    <h5 class="card-title">Экспорт</h5>
                                                    <p class="card-text">Экспорт из <?= esc($Essence['Country']); ?> в <?= esc($item['Country']); ?>: <?= $items['Export_From_Country1_To_Country2']; ?> трлн $ </p>
                                                    <p class="card-text">Экспорт из <?= esc($item['Country']); ?> в <?= esc($Essence['Country']); ?>: <?= $items['Export_From_Country2_To_Country1']; ?> трлн $ </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif ?>
                            <?php endforeach; ?>
                        <?php elseif ($items['Id_Country2'] == $Essence['ID_Country']) : ?>
                            <?php foreach ($Essence_new as $item): ?>
                                <?php if ($items['Id_Country1'] == $item['ID_Country'])  : ?>
                                    <div class="card mb-3" style="max-width: 540px;">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="card-body">
                                                    <h5 class="card-title">Экспорт</h5>
                                                    <p class="card-text">Экспорт из <?= esc($Essence['Country']); ?> в <?= esc($item['Country']); ?>: <?= $items['Export_From_Country1_To_Country2']; ?>трлн $ </p>
                                                    <p class="card-text">Экспорт из <?= esc($item['Country']); ?> в <?= esc($Essence['Country']); ?>: <?= $items['Export_From_Country2_To_Country1']; ?>трлн $ </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif ?>
                            <?php endforeach; ?>
                        <?php else : ?>
                        <?php endif ?>

                    <?php endforeach; ?>
            <?php else : ?>
                <p>Отсутствует информация об экспорте и импорте.</p>
                </div>

            <?php endif ?>
        </div>

    </div>
</div>

<?= $this->endSection() ?>
