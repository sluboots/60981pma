<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <?php if (!empty($Essence)) : ?>
            <?= form_open_multipart('country/store_GDP'); ?>
            <div class="form-group">
                <label for="name">Год</label>
                <input type="text" class="form-control <?= ($validation->hasError('YEAR')) ? 'is-invalid' : ''; ?>" name="YEAR"
                       value="<?= old('YEAR'); ?>">
                <div class="invalid-feedback">
                    <?= $validation->getError('YEAR') ?>
                </div>
            </div>
            <div class="form-group">
                <label for="name">ВВП</label>
                <input type="text" class="form-control <?= ($validation->hasError('GDP')) ? 'is-invalid' : ''; ?>" name="GDP"
                       value="<?= old('GDP'); ?>">
                <div class="invalid-feedback">
                    <?= $validation->getError('GDP') ?>
                </div>

            </div>
            <div class="form-group">
                <label for="name">ВВП на душу населения</label>
                <input type="text" class="form-control <?= ($validation->hasError('GDP_per_Person')) ? 'is-invalid' : ''; ?>" name="GDP_per_Person"
                       value="<?= old('GDP_per_Person'); ?>">
                <div class="invalid-feedback">
                    <?= $validation->getError('GDP_per_Person') ?>
                </div>

            </div>
            <input type="hidden" name="Id_Country" value="<?= $Essence[0]['ID_Country'] ?>">
            <div class="form-group">
                <button type="submit" class="btn btn-primary bg-dark " style="border-color: #222222" name="submit">Создать</button>
            </div>
            </form>
        <?php endif ?>

    </div>
<?= $this->endSection() ?>