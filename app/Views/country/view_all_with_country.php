<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php if (!empty($Essence) && is_array($Essence)) : ?>
            <h2>Все страны:</h2>
            <div class="d-flex justify-content-between mb-2">
                <?= $pager->links('group1', 'my_page') ?>
            </div>
            <?= form_open('country/viewAllWithCountry', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
            </form>
            <?= form_open('country/viewAllWithCountry',['style' => 'display: flex']); ?>
            <input type="text" class="form-control ml-3" name="search" placeholder="Имя или описание" aria-label="Search"
                   value="<?= $search; ?>">
            <button class="btn btn-outline-success" type="submit" class="btn btn-primary ">Найти</button>
            </form>
            <table class="table table-striped">
                <thead>
                <th scope="col">Аватар</th>
                <th scope="col">Название страны</th>
                <th scope="col">Email создателя</th>
                <th scope="col">Столица</th>
                <th scope="col">Управление</th>
                </thead>
                <tbody>
                <?php foreach ($Essence as $item): ?>
                    <tr>
                        <td>
                                <?php if ($item['Country'] == 'Russian Federation') : ?>
                                    <img src="https://img.icons8.com/emoji/96/000000/russia-emoji.png" class="card-img" style="margin-left: 25px" alt="<?= esc($item['Country']); ?>">
                                <?php elseif ($item['Country'] == 'USA'):?>
                                    <img src="https://img.icons8.com/cute-clipart/96/000000/usa.png" class="card-img" style="margin-left: 25px" alt="<?= esc($item['Country']); ?>">
                                <?php elseif ($item['Country'] == 'Switzerland'):?>
                                    <img src="https://img.icons8.com/emoji/96/000000/switzerland-emoji.png" class="card-img" style="margin-left: 25px" alt="<?= esc($item['Country']); ?>">
                                <?php elseif ($item['Country'] == 'Germany'):?>
                                    <img src="https://img.icons8.com/emoji/96/000000/germany-emoji.png" class="card-img" style="margin-left: 25px" alt="<?= esc($item['Country']); ?>">
                                <?php elseif ($item['Country'] == 'Canada'):?>
                                    <img src="https://img.icons8.com/emoji/96/000000/canada-emoji.png" class="card-img" style="margin-left: 25px" alt="<?= esc($item['Country']); ?>">
                                <?php elseif ($item['Country'] == 'Qatar'):?>
                                    <img src="https://img.icons8.com/emoji/96/000000/qatar-emoji.png" class="card-img" style="margin-left: 25px" alt="<?= esc($item['Country']); ?>">
                                <?php elseif ($item['Country'] == 'Kuwait'):?>
                                    <img src="https://img.icons8.com/emoji/96/000000/kuwait-emoji.png" class="card-img" style="margin-left: 25px" alt="<?= esc($item['Country']); ?>">
                                <?php else : ?>
                                    <img src="<?= esc($item['picture_Url']); ?>" class="card-img" style="margin-left: 25px; margin-top: 25px; min-width: 100px" alt="<?= esc($item['Country']); ?>">
                                <?php endif ?>
                        </td>
                        <td><?= esc($item['Country']); ?></td>
                        <td><?= esc($item['email']); ?></td>
                        <td><?= esc($item['Capital']); ?></td>
                        <td>
                            <a href="<?= base_url()?>/country/view/<?= esc($item['ID_Country']); ?>" class="btn btn-primary btn-sm bg-dark" style="color: white; border-color: #222222">Просмотреть</a>
                            <a href="<?= base_url()?>/country/edit/<?= esc($item['ID_Country']); ?>" class="btn btn-warning btn-sm bg-dark" style="color: white; border-color: #222222">Редактировать</a>
                            <a href='<?= base_url()?>/country/delete/<?= esc($item['ID_Country']); ?>' class="btn btn-danger btn-sm bg-dark" style="color: white; border-color: #222222">Удалить</a>
                        </td?
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        <?php else : ?>
            <div class="text-center ">
                <p>Страны не найдены </p>
                <a class="btn btn-primary btn-lg bg-dark" href="<?= base_url()?>/country/create"><span class="fas fa-tachometer-alt" style="color:white;"></span>&nbsp;&nbsp;Добавить страну</a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>