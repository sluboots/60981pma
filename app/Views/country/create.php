<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">
        <?php if (!empty($Essence)) : ?>

        <?= form_open_multipart('country/store'); ?>
        <div class="form-group">
            <label for="name">Страна</label>
            <input type="text" class="form-control <?= ($validation->hasError('Country')) ? 'is-invalid' : ''; ?>" name="Country"
                   value="<?= old('Country'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Country') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="name">Столица</label>
            <input type="text" class="form-control <?= ($validation->hasError('Capital')) ? 'is-invalid' : ''; ?>" name="Capital"
                   value="<?= old('Capital'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Capital') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="name">Популяция</label>
            <input type="text" class="form-control <?= ($validation->hasError('Population')) ? 'is-invalid' : ''; ?>" name="Population"
                   value="<?= old('Population'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Population') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="name">Площадь</label>
            <input type="text" class="form-control <?= ($validation->hasError('Area')) ? 'is-invalid' : ''; ?>" name="Area"
                   value="<?= old('Area'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Area') ?>
            </div>

        </div>
        <input type="hidden" name="ID_User" value="<?= $ionAuth->user()->row()->id ?>">
        <div class="form-group">
            <div class="form-group">
                <label for="birthday">Флаг страны</label>
                <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
                <div class="invalid-feedback">
                    <?= $validation->getError('picture') ?>
                </div>
            </div>
            <button type="submit" class="btn btn-primary bg-dark " style="border-color: #222222" name="submit">Создать</button>
        </div>
        </form>
    <?php endif ?>

    </div>
<?= $this->endSection() ?>