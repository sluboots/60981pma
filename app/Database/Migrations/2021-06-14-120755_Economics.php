<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Economics extends Migration
{
	public function up()
	{
        // activity_type
        if (!$this->db->tableexists('Essence'))
        {
            // Setup Keys
            $this->forge->addPrimarykey('ID_Country', TRUE);

            $this->forge->addfield(array(
                'ID_Country' => array('type' => 'INT', 'null' => FALSE, 'unsigned' => TRUE, 'auto_increment' => TRUE),
                'Country' => array('type' => 'VARCHAR', 'constraint' => '256', 'null' => FALSE),
                'Capital' => array('type' => 'VARCHAR','constraint' => '256', 'null' => FALSE),
                'Population' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'Area' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'ID_User' =>array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE)
                
            ));
            // create table
            $this->forge->createtable('Essence', TRUE);
        }

        if (!$this->db->tableexists('Trade'))
        {
            // Setup Keys
            $this->forge->addPrimarykey('ID_Trade', TRUE);

            $this->forge->addfield(array(
                'ID_Trade' =>array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'Id_Country1' => array('type' => 'INT','null' => FALSE, 'unsigned' => TRUE),
                'Id_Country2' => array('type' => 'INT','null' => FALSE, 'unsigned' => TRUE),
                'Export_From_Country1_To_Country2' => array('type' => 'INT','null' => FALSE, 'unsigned' => TRUE),
                'Export_From_Country2_To_Country1' => array('type' => 'INT','null' => FALSE, 'unsigned' => TRUE),
            ));
            $this->forge->addForeignKey('Id_Country1','Essence','ID_Country','RESTRICT','RESRICT');
            $this->forge->addForeignKey('Id_Country2','Essence','ID_Country','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('Trade', TRUE);
        }

        // activity_type
        if (!$this->db->tableexists('Economics'))
        {
            // Setup Keys
            $this->forge->addPrimarykey('ID_Economics', TRUE);

            $this->forge->addfield(array(
                'ID_Economics' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'Id_Country' => array('type' => 'INT', 'null' => FALSE, 'unsigned' => TRUE),
                'YEAR' => array('type' => 'INT','null' => FALSE, 'unsigned' => TRUE),
                'GDP' => array('type' => 'INT','null' => FALSE, 'unsigned' => TRUE),
                'GDP_per_Person' => array('type' => 'INT','null' => FALSE, 'unsigned' => TRUE)
            ));
            $this->forge->addForeignKey('Id_Country','Essence','ID_Country','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('Economics', TRUE);
        }
	}

	//--------------------------------------------------------------------

	public function down()
	{

         $this->forge->droptable('Essence');
         $this->forge->droptable('Trade');
         $this->forge->droptable('Economics');
	}
}

