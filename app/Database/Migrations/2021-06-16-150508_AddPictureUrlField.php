<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddPictureUrlField extends Migration
{
	public function up()
	{
		if ($this->db->tableexists('Essence'))
        {
            $this->forge->addColumn('Essence',array(
                'picture_Url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
	}

	public function down()
	{
		$this->forge->dropColumn('Essence', 'picture_url');
	}
}
