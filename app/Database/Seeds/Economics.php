<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Economics extends Seeder
{
	public function run()
	{

        $data = [
           
                'Country'=> 'Russian Federation',
                'Capital'=> 'Moscow',
                'Population' => 47125190,
                'Area' => 175678910,
                'ID_User' => 1
        ];
        $this->db->table('Essence')->insert($data);
        $data = [
           
                'Country'=> 'German',
                'Capital'=> 'Berlin',
                'Population' => 7125190,
                'Area' => 1678910,
                'ID_User' => 1
        ];
        $this->db->table('Essence')->insert($data);
       $data = [
           
                'Country'=> 'Qatar',
                'Capital'=> 'Qatar',
                'Population' => 125190,
                'Area' => 678910,
                'ID_User' => 1
        ];
        $this->db->table('Essence')->insert($data);
        $data = [
           
                'Country'=> 'Kuwait',
                'Capital'=> 'Kuwait-city',
                'Population' => 25190,
                'Area' => 78910,
                'ID_User' => 1
        ];
        $this->db->table('Essence')->insert($data);
        $data = [
           
                'Country'=> 'Switzerland',
                'Capital'=> 'Bern',
                'Population' => 435190,
                'Area' => 108910,
                'ID_User' => 1
        ];
        $this->db->table('Essence')->insert($data); 
        

       
        $data = [
           
                'Id_Country'=> 1,
                'YEAR'=> 2011,
                'GDP' => 121439,
                'GDP_per_Person' => 6433
        ];
        $this->db->table('Economics')->insert($data);

        $data = [
           
                'Id_Country'=> 2,
                'YEAR'=> 2020,
                'GDP' => '121433',
                'GDP_per_Person' => '6433'
        ];
        $this->db->table('Economics')->insert($data);

        $data = [
           
                'Id_Country'=> 3,
                'YEAR'=> 2021,
                'GDP' => '121433',
                'GDP_per_Person' => '6433'
        ];
        $this->db->table('Economics')->insert($data);

        $data = [
           
                'Id_Country'=> 1,
                'YEAR'=> 2021,
                'GDP' => '121433',
                'GDP_per_Person' => '6433'
        ];
        $this->db->table('Economics')->insert($data);


        $data = [
           
                'Id_Country1'=> 2,
                'Id_Country2'=> 1,
                'Export_From_Country1_To_Country2' => 121433,
                'Export_From_Country2_To_Country1' => 6433
        ];
        $this->db->table('Trade')->insert($data);
        $data = [
           
                'Id_Country1'=> 2,
                'Id_Country2'=> 3,
                'Export_From_Country1_To_Country2' => 121433,
                'Export_From_Country2_To_Country1' => 6433
        ];
        $this->db->table('Trade')->insert($data);
        $data = [
           
                'Id_Country1'=> 4,
                'Id_Country2'=> 5,
                'Export_From_Country1_To_Country2' => 121433,
                'Export_From_Country2_To_Country1' => 6433
        ];
        $this->db->table('Trade')->insert($data);
        
        
	}
}


